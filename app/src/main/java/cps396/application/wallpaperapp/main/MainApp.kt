package cps396.application.wallpaperapp.main

import android.app.Application
import android.content.Context
import android.net.Uri
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.assist.QueueProcessingType
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T





class MainApp: Application(), AnkoLogger {

    var uriList= ArrayList<Uri>()
    var favUri = ArrayList<Uri>()


    override fun onCreate() {

        super.onCreate()

        info("Wallpaper App started")
        initImageLoader(applicationContext)
    }

    //Initiate Image Loader Configuration to load images faster
    fun initImageLoader(context: Context) {
        val config = ImageLoaderConfiguration.Builder(
            context
        )
        config.threadPriority(Thread.NORM_PRIORITY - 2)
        config.denyCacheImageMultipleSizesInMemory()
        config.diskCacheFileNameGenerator(Md5FileNameGenerator())
        config.diskCacheSize(50 * 1024 * 1024) // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO)
        config.writeDebugLogs() // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build())

    }

}