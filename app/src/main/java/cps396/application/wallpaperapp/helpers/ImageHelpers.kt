package cps396.application.wallpaperapp.helpers

import android.app.Activity
import android.content.Context

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.ViewGroup
import android.widget.GridView

import androidx.core.content.FileProvider
import cps396.application.wallpaperapp.R
import cps396.application.wallpaperapp.activities.GalleryAdapter
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.startActivityForResult
import java.io.File
import java.io.IOException
import java.net.URI
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger
import kotlin.collections.ArrayList


fun showImagePicker(parent: Activity, id: Int) {

    val intent = Intent()
    intent.type = "image/*"
    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)

    intent.action = Intent.ACTION_OPEN_DOCUMENT
    intent.addCategory(Intent.CATEGORY_OPENABLE)
    val chooser = Intent.createChooser(intent, R.string.select_background_image.toString())
    parent.startActivityForResult(chooser, id)

}

fun readImageFromUri(context: Context, uri : Uri) : Bitmap? {
    var bitmap : Bitmap? = null

    if (uri != null) {
        try {
            val parcelFileDescriptor = context.contentResolver.openFileDescriptor(uri, "r")
            val fileDescriptor = parcelFileDescriptor!!.fileDescriptor
            bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
            parcelFileDescriptor.close()
        } catch (e: Exception) {
        }
    }
    return bitmap
}

fun setUpGridView(context: Context, gvGallery:GridView,uriList: List<Uri>){

    val galleryAdapter = GalleryAdapter(context, uriList)
    gvGallery.adapter = galleryAdapter
    gvGallery.verticalSpacing = gvGallery.horizontalSpacing
    val mlp = gvGallery.layoutParams as ViewGroup.MarginLayoutParams

    mlp.setMargins(0, gvGallery.horizontalSpacing, 0, 0)
}

//shuffle selected images and display randomized images, return updated uri list
fun displayRandomized(context: Context,gvGallery: GridView,uriList: List<Uri>): List<Uri>{

    val newUriList = uriList.shuffled()
    setUpGridView(context,gvGallery,newUriList)
    return newUriList
}

fun takePictureIntent(parent: Activity, id: Int){

    Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
        // Ensure that there's a camera activity to handle the intent
        takePictureIntent.resolveActivity(parent.packageManager)?.also {
            // Create the File where the photo should go
            val photoFile: File? = try {
                createImageFile(parent)
            } catch (ex: IOException) {
                // Error occurred while creating the File
                null
            }
            // Continue only if the File was successfully created
            photoFile?.also {
                val photoURI: Uri = FileProvider.getUriForFile(
                    parent.applicationContext,
                    "cps396.application.wallpaperapp.fileprovider",
                    it
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                takePictureIntent.putExtra("PHOTO_PATH", photoFile.absolutePath)

                parent.startActivityForResult(takePictureIntent, id)
            }


        }
    }
}
    var currentPhotoPath: String = ""

fun getPhotoPath():String{
    return currentPhotoPath
}


@Throws(IOException::class)
fun createImageFile(parent: Activity): File {

    //create an image file name
    val timeStamp:String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    var storageDir = parent.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    return File.createTempFile("JPEG_${timeStamp}_",".jpg",storageDir
    ).apply {
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = absolutePath
    }

}