package cps396.application.wallpaperapp.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.widget.GridView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import cps396.application.wallpaperapp.R
import cps396.application.wallpaperapp.helpers.displayRandomized
import cps396.application.wallpaperapp.helpers.getPhotoPath
import cps396.application.wallpaperapp.helpers.setUpGridView
import cps396.application.wallpaperapp.helpers.takePictureIntent
import cps396.application.wallpaperapp.main.MainApp
import kotlinx.android.synthetic.main.activity_display.*
import kotlinx.android.synthetic.main.activity_display.btn_randomize
import kotlinx.android.synthetic.main.activity_display.btn_setTime
import kotlinx.android.synthetic.main.activity_display.toolbarDisplay
import kotlinx.android.synthetic.main.activity_photo.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast
import java.io.File

class TakePhotosActivity:AppCompatActivity(), AnkoLogger {
    val REQUEST_IMAGE_CAPTURE = 1
    lateinit var app:MainApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)

        app = application as MainApp

        //set toolbar title
        toolbarDisplay.title = title
        setSupportActionBar(toolbarDisplay)

        btn_MorePhotos.setOnClickListener {
            takePictureIntent(this,REQUEST_IMAGE_CAPTURE)
        }

        //randomize selected images
        btn_randomize.setOnClickListener{
            val gvGallery = findViewById<GridView>(R.id.gv)
            val newUriList = displayRandomized(this,gvGallery,app.uriList)
            app.uriList = ArrayList(newUriList)
        }

        btn_setTime.setOnClickListener{
            val st_intent = Intent(this, SetTimeActivity::class.java)
            startActivity(st_intent)
        }

        val intent = this.intent

        //Find View By Id For GridView
        val gvGallery = findViewById<GridView>(R.id.gv)
        //one photo
        var imageUri = intent.getParcelableExtra<Uri>("PHOTO_URI")
        app.uriList.add(imageUri)
        if(imageUri ==null){
            info("null")
        }
        //set up view
        setUpGridView(this,gvGallery,app.uriList)
        //Log.e("photo taken: ", imageUri.toString())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_nav_display, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.home -> startActivityForResult<WelcomeActivity>(0)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode ==REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK){
            //val photoIntent = Intent(this,TakePhotosActivity::class.java)

            val file = File(getPhotoPath())
            val photoUri = Uri.fromFile(file)
            info("pHOTO uri: $photoUri")

            //Find View By Id For GridView
            val gvGallery = findViewById<GridView>(R.id.gv)

            app.uriList.add(photoUri)
            if(photoUri ==null){
                info(" Photo taken null")
            }
            //set up view
            setUpGridView(this,gvGallery,app.uriList)
        }
        else {
            toast("Failed to take photo(s)")
        }
    }
}