package cps396.application.wallpaperapp.activities


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.core.app.ActivityCompat
import cps396.application.wallpaperapp.R
import cps396.application.wallpaperapp.helpers.getPhotoPath
import cps396.application.wallpaperapp.helpers.showImagePicker
import cps396.application.wallpaperapp.helpers.takePictureIntent
import cps396.application.wallpaperapp.main.MainApp
import kotlinx.android.synthetic.main.activity_home.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.toast
import java.io.File


class HomeActivity : AppCompatActivity(), AnkoLogger {


    lateinit var app: MainApp

    val REQUEST_IMAGE_CAPTURE = 2


    companion object {

        private val PERMISSION_CODE = 1001;
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        info("Home Activity started..")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        toolbarSelect.title = title
        setSupportActionBar(toolbarSelect)

        app = application as MainApp

        chooseImage.setOnClickListener{

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE)
                }
                else{
                    //permission already granted
                    info("Browse image")
                    //showImagePicker(this,PICK_IMAGE_MULTIPLE)
                    startActivity(Intent(this,DisplayActivity::class.java))
                }
            }
            else{
                //system OS is < Marshmallow
                info("Browse image")
                //showImagePicker(this,PICK_IMAGE_MULTIPLE)
                startActivity(Intent(this,DisplayActivity::class.java))
            }
        }


        takePhotos.setOnClickListener {
            //startActivity(Intent(this,TakePhotosActivity::class.java))

           info ("Take photos")
            takePictureIntent(this,REQUEST_IMAGE_CAPTURE)
        }



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode ==REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK){
            val photoIntent = Intent(this,TakePhotosActivity::class.java)

            val file = File(getPhotoPath())
            val photoUri = Uri.fromFile(file)
            info("pHOTO uri: $photoUri")
            photoIntent.putExtra("PHOTO_URI",photoUri)
            startActivity(photoIntent)
        }
        else {
            toast("Failed to take photo(s)")
            }
        }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size >0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    startActivity(Intent(this,DisplayActivity::class.java))
                }
                else{
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    }



