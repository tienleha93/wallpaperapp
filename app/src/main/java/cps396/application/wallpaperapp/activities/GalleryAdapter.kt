package cps396.application.wallpaperapp.activities


import android.app.Activity
import android.content.Context
import android.content.Intent
import cps396.application.wallpaperapp.R
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import cps396.application.wallpaperapp.main.MainApp


class GalleryAdapter(var context: Context,  var mArrayUri: List<Uri>) :
    BaseAdapter(){

    //var context = context
    //var mArrayUri = mArrayUri


    private var pos:Int = 0
    private var favIdList =ArrayList<Int>()


    override fun getCount(): Int {
        return mArrayUri.size
    }

    override fun getItem(position: Int): Any {
        return mArrayUri[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var myView = convertView
        val holder: ViewHolder

        pos = position
        //time = date

        if(myView==null) {

            //if view is null, inflate view using layoutInflater
            val inflater = (context as Activity).layoutInflater

            //inflate gv_item
            myView = inflater.inflate(R.layout.gv_item, parent, false)

            //create Object of ViewHolder class and set view to it
            holder = ViewHolder()

            //holder.mCheck = myView!!.findViewById(R.id.itemCheckbox) as CheckBox
            holder.mImageView = myView!!.findViewById(R.id.imageView) as ImageView

            //tag to identify view
            myView.tag =holder
        }
        else {
            //if view is not null
            holder = myView.tag as ViewHolder
        }

        holder.mImageView!!.setImageURI(mArrayUri[position])

       // holder.mCheck!!.id = position
        //holder.mImageView!!.id = position


        return myView
    }

    class ViewHolder {

        var mImageView: ImageView? = null
        //var mCheck: CheckBox? = null


    }

}

