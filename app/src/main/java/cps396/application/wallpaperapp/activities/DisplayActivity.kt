package cps396.application.wallpaperapp.activities


import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.GridView
import androidx.appcompat.app.AppCompatActivity
import cps396.application.wallpaperapp.R
import cps396.application.wallpaperapp.helpers.displayRandomized
import cps396.application.wallpaperapp.helpers.setUpGridView
import cps396.application.wallpaperapp.helpers.showImagePicker
import cps396.application.wallpaperapp.main.MainApp
import kotlinx.android.synthetic.main.activity_display.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast


class DisplayActivity:AppCompatActivity(), AnkoLogger {


    lateinit var app: MainApp

    companion object{
        val PICK_IMAGE_MULTIPLE = 1
        lateinit var imageEncoded:String
        var imagesEncodedList: MutableList<String> = arrayListOf()

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display)

        app = application as MainApp

        //set toolbar title
        toolbarDisplay.title = title
        setSupportActionBar(toolbarDisplay)



        showImagePicker(this,PICK_IMAGE_MULTIPLE)

        //randomize selected images
        btn_randomize.setOnClickListener{
            val gvGallery = findViewById<GridView>(R.id.gv)
            val newUriList = displayRandomized(this,gvGallery,app.uriList)
            app.uriList = ArrayList(newUriList)
        }

        btn_setTime.setOnClickListener{
            val st_intent = Intent(this, SetTimeActivity::class.java)
            startActivity(st_intent)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_nav_display, menu)
        return super.onCreateOptionsMenu(menu)
    }

    //back to home
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.home -> startActivityForResult<WelcomeActivity>(0)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //Find View By Id For GridView
        val gvGallery = findViewById<GridView>(R.id.gv)

        if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == Activity.RESULT_OK
            && null != data) {

            val clipData = data.clipData

            if (clipData != null) { //multiple photos

                for (i in 0 until clipData.itemCount) {

                    val imageUri = clipData.getItemAt(i).uri
                    Log.e("multiple image, no.: $i ", imageUri.toString())

                    app.uriList.add(imageUri)
                    getPathFromURI(imageUri)

                    //set up view
                    setUpGridView(this,gvGallery,app.uriList)
                }
            } else {
                val uri = data.data         //single photo

                if (uri != null) {

                    //add to uri list
                    app.uriList.add(uri)

                    //set up view
                    setUpGridView(this,gvGallery,app.uriList)
                    Log.e("single image: ", uri.toString())
                }
            }
        }
        else {
            toast("You haven't picked image")
        }
    }



    fun getPathFromURI(uri: Uri){

        val path = uri.path.toString()


        val databaseUri: Uri
        val selection: String?
        val selectionArgs: Array<String>?
        if (path.contains("/document/image:")) { // files selected from "Documents"
            databaseUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            selection = "_id=?"
            selectionArgs = arrayOf(DocumentsContract.getDocumentId(uri).split(":")[1])
        } else { // files selected from all other sources
            databaseUri = uri
            selection = null
            selectionArgs = null
        }
        try {
            //data to query
            val projection = arrayOf(
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.ORIENTATION,
                MediaStore.Images.Media.DATE_TAKEN
            )

            val resolver: ContentResolver = contentResolver
            val cursor = resolver.query(databaseUri, projection, selection, selectionArgs, null)

            if(cursor!!.moveToFirst()){
                val columnIndex = cursor.getColumnIndex(projection[0])
                imageEncoded = cursor.getString(columnIndex)
                // Log.e("path", imagePath);
                imagesEncodedList.add(imageEncoded)

            }
                cursor.close()


        } catch (e: Exception) {
            //Log.e(TAG, e.message, e)
        }

    }


    fun getUri(): List<Uri>{
        return app.uriList
    }
}







