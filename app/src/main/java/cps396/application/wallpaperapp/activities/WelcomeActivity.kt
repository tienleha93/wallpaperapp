package cps396.application.wallpaperapp.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cps396.application.wallpaperapp.R
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        WelcomeButton.setOnClickListener{
            startActivity( Intent(this, HomeActivity::class.java))

        }
    }

}