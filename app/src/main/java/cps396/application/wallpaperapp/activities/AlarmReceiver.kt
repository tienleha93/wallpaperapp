package cps396.application.wallpaperapp.activities

import android.app.Activity
import android.app.WallpaperManager
import android.content.*

import android.net.Uri

import android.util.Log
import android.widget.Toast

import java.util.*

import cps396.application.wallpaperapp.helpers.readImageFromUri

import android.content.SharedPreferences



//receiver
class AlarmReceiver:BroadcastReceiver() {


    override fun onReceive(context: Context?, intent: Intent?) {


        Log.d("Alarm Receiver","Receiver : " + Date().toString())

        val context = context!!.applicationContext

        var uriList = intent!!.getSerializableExtra("uriList") as ArrayList<Uri>

        var imgNum = uriList.size // #image in uriList
        Log.d("#imageList","#image : $imgNum")

        //to store index of next image
        var values:SharedPreferences = context.getSharedPreferences("count_times", Activity.MODE_PRIVATE)
        var idx = values.getInt("index",0)

        Log.d("index","idx : $idx")
        if(idx ==imgNum){
            Toast.makeText(context, "All selected images are set. Start a new cycle!", Toast.LENGTH_LONG).show()
            idx = 0
        }

        try{
            val wpManager: WallpaperManager = WallpaperManager.getInstance(context)
            wpManager.setBitmap(readImageFromUri(context,uriList[idx]))
            Toast.makeText(context, "Wallpaper set", Toast.LENGTH_LONG).show()

            //update index for next receiver
            idx++

            //Write updated index back to storage for later use
            val editor: SharedPreferences.Editor  = values.edit()
            editor.putInt("index", idx)
            editor.commit()
        }
        catch (e: Exception){
            Toast.makeText(context, "Wallpaper failed", Toast.LENGTH_LONG).show()
            Log.e(ContentValues.TAG, e.message, e)
        }
    }


}