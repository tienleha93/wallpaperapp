package cps396.application.wallpaperapp.activities

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import cps396.application.wallpaperapp.main.MainApp
import cps396.application.wallpaperapp.R
import kotlinx.android.synthetic.main.activity_settime.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.util.*


class SetTimeActivity: AppCompatActivity(), AnkoLogger, AdapterView.OnItemSelectedListener{

    private lateinit var context: Context
    private lateinit var pendingIntent:PendingIntent
    private lateinit var alarmManager: AlarmManager
    lateinit var app:MainApp
    private var interval: String =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settime)

        app= application as MainApp
        context = this
        alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        //set up spinner
        var spinner = findViewById<Spinner>(R.id.timeSpinner)

        // Initializing a String Array
        // Initializing a String Array
        val timeArray = arrayOf(
            "Minutes",
            "Hours",
            "Days",
            "Week"
        )
        // Create an ArrayAdapter

        val spinnerAdapter: ArrayAdapter<String> = ArrayAdapter<String>(
            this, R.layout.spinner_item, timeArray
        )
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = spinnerAdapter
        spinner.onItemSelectedListener = this

        btnChange.setOnClickListener {

            //get time
            var seconds:Long = 0
            when(interval){
                "Minutes" -> seconds = 60 *1000
                "Hours" -> seconds = AlarmManager.INTERVAL_HOUR
                "Days" -> seconds = AlarmManager.INTERVAL_DAY
                "Weeks" -> seconds = 604800 *1000
            }

            val timeInput = edt_timeInput.text.toString().toLong() *seconds
            //create pending intent to send to the Broadcast Receiver

            val intent = Intent(applicationContext,AlarmReceiver::class.java)

            var times: SharedPreferences = context.getSharedPreferences("count_times", Activity.MODE_PRIVATE)
            val editor: SharedPreferences.Editor  = times.edit()
            editor.putInt("index",0)
            editor.apply()

            intent.putExtra("uriList",app.uriList)

            pendingIntent = PendingIntent.getBroadcast(applicationContext,0,intent,PendingIntent.FLAG_UPDATE_CURRENT)
            Log.d("SetTimeActivity", "create: " + Date().toString())
            alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis(),timeInput,pendingIntent)
        }

        btnCancel.setOnClickListener {
            startActivity(Intent(this,HomeActivity::class.java))
        }



    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        interval = parent!!.getItemAtPosition(position).toString()
        info("Change wallpaper between: $interval")
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}