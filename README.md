# WallpaperApp

Team members:
1. Tien Le Cmich Id: le1tt
2. Namuunbadralt Zolboot ID: zolbo1n
3. Daniel Barajas ID: baraj1d

References:
https://www.youtube.com/watch?v=XTgg7DybmRI

https://tutors-design.netlify.com/topic/wit-cmu-mobile-app-dev-2019.netlify.com/topic-05-anatomy2-activities

https://tutors-design.netlify.com/lab/wit-cmu-mobile-app-dev-2019.netlify.com/topic-08-images/unit-01-android-programming/book-04-images

https://github.com/p4yam/SmallWall 

https://developer.android.com/training/scheduling/alarms
https://developer.android.com/training/camera/photobasics#TaskPhotoView
https://developer.android.com/reference/android/support/v4/content/FileProvider#GetUri

Video link:

https://youtu.be/blBxS1x8DYs

Milestones 
*Adjust image size to wallpaper size
*Add Navigation
*Add Favorite folder
*Add Camera 
*Add taken images to Gallery
*Add Time interval
*Add Permissions
*Add suitable UI
*Add text of location and date of the image to the wallpaper


https://wit-cmu-mobile-dev-19.slack.com/archives/CQJR4PJCD/p1574369792004400


